import pandas as pd
import numpy as np

def B2B_Comparison(Productivity, Profitability, timeline, District_A, Branch_A, District_B, Branch_B):
    temp = pd.DataFrame()
    year = timeline[0:4]
    
    # code for scatter plot
    
    temp_df_prod1 = df_prod[(df_prod['Date (YYYYMM)'] == int(timeline)) & (df_prod['Sales Office'] == Branch_A)]
    temp_df_prod2 = df_prod[(df_prod['Date (YYYYMM)'] == int(timeline)) & (df_prod['Sales Office'] == Branch_B)]
    temp_df_prod = pd.concat([temp_df_prod1, temp_df_prod2])
    temp_df_prof1 = df_prof[(df_prof['Date (YYYYMM)'] == int(timeline)) & (df_prof['Sales Office'] == Branch_A)]
    temp_df_prof2 = df_prof[(df_prof['Date (YYYYMM)'] == int(timeline)) & (df_prof['Sales Office'] == Branch_B)]
    temp_df_prof = pd.concat([temp_df_prof1, temp_df_prof2])

    temp['Legend'] = temp_df_prod[Productivity+' Alpha'] + temp_df_prof[Profitability+' Alpha']
    
    colors=['#d6604d', '#4393c3']
    branches = [Branch_A, Branch_B]
    colormap = dict(zip(branches, colors))
    temp['Colors'] = [colormap[x] for x in branches]
    
    df = pd.DataFrame({'Sales_Office' : temp_df_prod['Sales Office'], 
                       'Date_(YYYYMM)': temp_df_prod['Date (YYYYMM)'], 
                       'Month' :temp_df_prod['Month'],
                       Productivity+'_Prod': temp_df_prod[Productivity],
                       Profitability+'_Prof': temp_df_prof[Profitability],
                       'Legend': temp_df_prod[Productivity+' Alpha'] + temp_df_prof[Profitability+' Alpha'],
                       'Colors': temp['Colors']})

    
    #code for trend lines
    
    df_prod1 = df_prod[(df_prod['Year'] == int(year)) & (df_prod['Sales Office'] == str(Branch_A))]
    df_prod2 = df_prod[(df_prod['Year'] == int(year)) & (df_prod['Sales Office'] == str(Branch_B))]
    df_prof1 = df_prof[(df_prof['Year'] == int(year)) & (df_prof['Sales Office'] == str(Branch_A))]
    df_prof2 = df_prof[(df_prof['Year'] == int(year)) & (df_prof['Sales Office'] == str(Branch_B))]
    
    if year !='2019':
        factors = [("Q1", "Jan"), ("Q1", "Feb"), ("Q1", "Mar"),
                   ("Q2", "Apr"), ("Q2", "May"), ("Q2", "Jun"),
                   ("Q3", "Jul"), ("Q3", "Aug"), ("Q3", "Sep"),
                   ("Q4", "Oct"), ("Q4", "Nov"), ("Q4", "Dec")]
    else :
        factors = [("Q1", "Jan"), ("Q1", "Feb"), ("Q1", "Mar"),
                   ("Q2", "Apr"), ("Q2", "May"), ("Q2", "Jun"),
                   ("Q3", "Jul"),]

    df1 = pd.DataFrame({'x' : factors, 'Branches' : df_prod1['Sales Office'], 'Year' :df_prod1['Year'], 
                        'Month' :df_prod1['Month'], 'Productivity': df_prod1[Productivity],
                        'Profitability': df_prof1[Profitability], 
                        "Total_Score" : (df_prod1[Productivity] + df_prof1[Profitability])
                                       })
    df2 = pd.DataFrame({'x' : factors, 'Branches' : df_prod2['Sales Office'], 'Year' :df_prod2['Year'], 
                        'Month' :df_prod2['Month'], 'Productivity': df_prod2[Productivity],
                        'Profitability': df_prof2[Profitability], 
                        "Total_Score" : (df_prod2[Productivity] + df_prof2[Profitability])})

   