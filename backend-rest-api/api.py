import flask
import process_data
from flask import request,  jsonify, make_response

app = flask.Flask(__name__)


@app.route('/v1/branch-to-branch-comparison/', methods=["GET"])
def branch_to_branch_comparison():

    branch_a_name = request.args.get('branch_a_name')

    branch_b_name = request.args.get('branch_b_name')

    district_a_name = request.args.get('district_a_name')

    district_b_name = request.args.get('district_b_name')

    productivity_scenario_type = request.args.get('productivity_scenario_type')

    profitability_scenario_type = request.args.get('profitability_scenario_type')

    month = request.args.get('month')

    year = request.args.get('year')

    response_body = {

        "branch_a_profitability": process_data.get_profitability(branch_a_name, district_a_name, month, year, profitability_scenario_type),

        "branch_a_productivity" : process_data.get_productivity(branch_a_name, district_a_name, month, year, productivity_scenario_type),
        
        "branch_b_profitability": process_data.get_profitability(branch_b_name, district_b_name, month, year, profitability_scenario_type),

        "branch_b_productivity": process_data.get_productivity(branch_b_name, district_b_name, month, year, productivity_scenario_type),
        
    }

    res = make_response(jsonify(response_body), 200)
    
    return res


@app.route('/v1/single-branch-comparison/', methods=["GET"])
def single_branch_comparison():

    branch_name = request.args.get('branch_name')

    district_name = request.args.get('district_name')

    productivity_scenario_type = request.args.get('productivity_scenario_type')

    profitability_scenario_type = request.args.get('profitability_scenario_type')

    month = request.args.get('month')

    year = request.args.get('year')

    response_body = {

        "productivity_factors": {
            "sales_district_name" : "",
            "sales_office" : "",
            "date" : "",
            "sales_per_employee": "",
            "gm_per_employee": "",
            "sales_order_per_employee": "",
            "sales_order_delivery_per_employee": "",
            "non_sales_order_delivery_per_employee": "",
            "delivery_lines_per_employee": "",
            "delivery_docs_per_employee": "",
            "employee_count": ""
        },

        "profitability_factors": {
            "sales_district_name" : "",
            "sales_office" : "",
            "date" : "",
            "num_delivery_lines": "",
            "sales_per_branch": "",
            "gm_per_branch": "",
            "rso_by_plant": "",
            "gmr": "",
            "sales_order_rso": ""
        }
    }

    res = make_response(jsonify(response_body), 200)
    
    return res
