import pandas as pd
import numpy as np

# Load computed csv values
df_prod_factors = pd.read_csv("./Data/Productivity_Factors.csv", index_col=0)
df_prod_grades_alpha = pd.read_csv("./Data/Productivity_Grades_Alpha.csv", index_col=0)
df_prod_grades_num = pd.read_csv("./Data/Productivity_Grades_Num.csv", index_col=0)
df_prof_factors = pd.read_csv("./Data/Profitability_Factors.csv", index_col=0)
df_prof_grades_alpha = pd.read_csv("./Data/Profitability_Grades_Alpha.csv", index_col=0, dtype={'High GMR': object, 'High Sales and GM Weight': object, 'Equal Weights': object})
df_prof_grades_num = pd.read_csv("./Data/Profitability_Grades_Num.csv", index_col=0)

# Load 
df_prod = df_prod_grades_num[['Sales District Name', 'Sales Office', 'Date (YYYYMM)','High Sales Weight', 'Equal Sales and GM Weight', 'Equal Weights']].copy()
df_prod[['High Sales Weight Alpha', 'Equal Sales and GM Weight Alpha', 'Equal Weights Alpha']] = df_prod_grades_alpha[['High Sales Weight', 'Equal Sales and GM Weight', 'Equal Weights']].copy()
df_prod[['GM per Emp','Sales Order per Emp', 'Sales Order Items per Emp',\
         'Sales Order Delivery per Emp', 'Non-Sales Order Delivery per Emp',\
         'Delivery Lines per Emp', 'Deliver Docs per Emp', 'RSO per Emp','Sales per Emp', 'Employee Count']] = df_prod_factors[['GM per Emp','Sales Order per Emp', 'Sales Order Items per Emp',\
         'Sales Order Delivery per Emp', 'Non-Sales Order Delivery per Emp',\
         'Delivery Lines per Emp', 'Deliver Docs per Emp', 'RSO per Emp','Sales per Emp', 'Employee Count']].copy()
df_prod['Year'] = df_prod['Date (YYYYMM)'].astype(str).str[0:4].astype(int)
df_prod['Month'] = df_prod['Date (YYYYMM)'].astype(str).str[4:].astype(int)

df_prof = df_prof_grades_num[['Sales District Name', 'Sales Office', 'Date (YYYYMM)','Equal Weights', 'High Sales and GM Weight', 'High GMR']].copy()
df_prof[['High GMR Alpha', 'High Sales and GM Weight Alpha','Equal Weights Alpha']] = df_prof_grades_alpha[['High GMR', 'High Sales and GM Weight','Equal Weights']].copy()
df_prof[['Number of Delivery Lines (SO)', 'Sales $ per branch','GM $ per branch', 'RSO by plant', 'GMR', 'Sales Order - RSO']] = df_prof_factors[['Number of Delivery Lines (SO)', 'Sales $ per branch',\
       'GM $ per branch', 'RSO by plant', 'GMR', 'Sales Order - RSO']].copy()
df_prof['Year'] = df_prof['Date (YYYYMM)'].astype(str).str[0:4].astype(int)
df_prof['Month'] = df_prof['Date (YYYYMM)'].astype(str).str[4:].astype(int)

dates = ['201701', '201702', '201703', '201704', '201705', '201706', '201707', 
         '201708', '201709', '201710', '201711', '201712', '201801', '201802', 
         '201803', '201804', '201805', '201806', '201807', '201808', '201809', 
         '201810', '201811', '201812', '201901', '201902', '201903', '201904',
         '201905', '201906', '201907']

Years = ['2019', '2018', '2017']

branches = df_prod['Sales Office'].unique()
district = df_prof['Sales District Name'].unique()

district_branch = dict(zip(df_prod['Sales Office'], df_prof['Sales District Name']))




def B2B_Comparison(Productivity, Profitability, timeline, District_A, Branch_A, District_B, Branch_B):
    temp = pd.DataFrame()
    year = timeline[0:4]
    
    # code for scatter plot
    
    temp_df_prod1 = df_prod[(df_prod['Date (YYYYMM)'] == int(timeline)) & (df_prod['Sales Office'] == Branch_A)]
    temp_df_prod2 = df_prod[(df_prod['Date (YYYYMM)'] == int(timeline)) & (df_prod['Sales Office'] == Branch_B)]
    temp_df_prod = pd.concat([temp_df_prod1, temp_df_prod2])
    temp_df_prof1 = df_prof[(df_prof['Date (YYYYMM)'] == int(timeline)) & (df_prof['Sales Office'] == Branch_A)]
    temp_df_prof2 = df_prof[(df_prof['Date (YYYYMM)'] == int(timeline)) & (df_prof['Sales Office'] == Branch_B)]
    temp_df_prof = pd.concat([temp_df_prof1, temp_df_prof2])

    temp['Legend'] = temp_df_prod[Productivity+' Alpha'] + temp_df_prof[Profitability+' Alpha']
    
    colors=['#d6604d', '#4393c3']
    branches = [Branch_A, Branch_B]
    colormap = dict(zip(branches, colors))
    temp['Colors'] = [colormap[x] for x in branches]
    
    df = pd.DataFrame({'Sales_Office' : temp_df_prod['Sales Office'], 
                       'Date_(YYYYMM)': temp_df_prod['Date (YYYYMM)'], 
                       'Month' :temp_df_prod['Month'],
                       Productivity+'_Prod': temp_df_prod[Productivity],
                       Profitability+'_Prof': temp_df_prof[Profitability],
                       'Legend': temp_df_prod[Productivity+' Alpha'] + temp_df_prof[Profitability+' Alpha'],
                       'Colors': temp['Colors']})

    
    #code for trend lines
    
    df_prod1 = df_prod[(df_prod['Year'] == int(year)) & (df_prod['Sales Office'] == str(Branch_A))]
    df_prod2 = df_prod[(df_prod['Year'] == int(year)) & (df_prod['Sales Office'] == str(Branch_B))]
    df_prof1 = df_prof[(df_prof['Year'] == int(year)) & (df_prof['Sales Office'] == str(Branch_A))]
    df_prof2 = df_prof[(df_prof['Year'] == int(year)) & (df_prof['Sales Office'] == str(Branch_B))]
    
    if year !='2019':
        factors = [("Q1", "Jan"), ("Q1", "Feb"), ("Q1", "Mar"),
                   ("Q2", "Apr"), ("Q2", "May"), ("Q2", "Jun"),
                   ("Q3", "Jul"), ("Q3", "Aug"), ("Q3", "Sep"),
                   ("Q4", "Oct"), ("Q4", "Nov"), ("Q4", "Dec")]
    else :
        factors = [("Q1", "Jan"), ("Q1", "Feb"), ("Q1", "Mar"),
                   ("Q2", "Apr"), ("Q2", "May"), ("Q2", "Jun"),
                   ("Q3", "Jul"),]

    df1 = pd.DataFrame({'x' : factors, 'Branches' : df_prod1['Sales Office'], 'Year' :df_prod1['Year'], 
                        'Month' :df_prod1['Month'], 'Productivity': df_prod1[Productivity],
                        'Profitability': df_prof1[Profitability], 
                        "Total_Score" : (df_prod1[Productivity] + df_prof1[Profitability])
                                       })
    df2 = pd.DataFrame({'x' : factors, 'Branches' : df_prod2['Sales Office'], 'Year' :df_prod2['Year'], 
                        'Month' :df_prod2['Month'], 'Productivity': df_prod2[Productivity],
                        'Profitability': df_prof2[Profitability], 
                        "Total_Score" : (df_prod2[Productivity] + df_prof2[Profitability])})

    return df


def B2B_Comparison_Tables(Productivity, Profitability, timeline, Year, District_A, Branch_A, District_B, Branch_B):

        prod_columns = ['Sales District Name', 'Sales Office', 'Date (YYYYMM)', 'Sales per Emp',
                        'GM per Emp', 'Sales Order per Emp', 'Sales Order Items per Emp', 
                        'Sales Order Delivery per Emp','Non-Sales Order Delivery per Emp', 
                        'Delivery Lines per Emp', 'Deliver Docs per Emp', 'Employee Count']
        prof_columns = ['Sales District Name', 'Sales Office', 'Date (YYYYMM)',
                        'Number of Delivery Lines (SO)','Sales $ per branch',
                        'GM $ per branch', 'RSO by plant','GMR','Sales Order - RSO']

#   Productivity Yearly Comparison Code

        prod_agg_columns = ['Sales District Name', 'Sales Office', 'Year', 'Sales per Emp',
                            'GM per Emp', 'Sales Order per Emp', 'Sales Order Items per Emp', 
                            'Sales Order Delivery per Emp','Non-Sales Order Delivery per Emp', 
                            'Delivery Lines per Emp', 'Deliver Docs per Emp', 'Employee Count']

        agg1=[]
        agg1.append(str(District_A))
        agg1.append(str(Branch_A))
        agg1.append(str(Year))
        agg1.append(df_prod.loc[(df_prod['Year'] == int(Year)) & (df_prod['Sales Office'] == str(Branch_A)), 'Sales per Emp'].sum())
        agg1.append(df_prod.loc[(df_prod['Year'] == int(Year)) & (df_prod['Sales Office'] == str(Branch_A)), 'GM per Emp'].sum())
        agg1.append(df_prod.loc[(df_prod['Year'] == int(Year)) & (df_prod['Sales Office'] == str(Branch_A)), 'Sales Order per Emp'].sum())
        agg1.append(df_prod.loc[(df_prod['Year'] == int(Year)) & (df_prod['Sales Office'] == str(Branch_A)), 'Sales Order Items per Emp'].sum())
        agg1.append(df_prod.loc[(df_prod['Year'] == int(Year)) & (df_prod['Sales Office'] == str(Branch_A)), 'Sales Order Delivery per Emp'].sum())
        agg1.append(df_prod.loc[(df_prod['Year'] == int(Year)) & (df_prod['Sales Office'] == str(Branch_A)), 'Non-Sales Order Delivery per Emp'].sum())
        agg1.append(df_prod.loc[(df_prod['Year'] == int(Year)) & (df_prod['Sales Office'] == str(Branch_A)), 'Delivery Lines per Emp'].sum())
        agg1.append(df_prod.loc[(df_prod['Year'] == int(Year)) & (df_prod['Sales Office'] == str(Branch_A)), 'Deliver Docs per Emp'].sum())
        agg1.append(df_prod.loc[(df_prod['Year'] == int(Year)) & (df_prod['Sales Office'] == str(Branch_A)), 'Employee Count'].mean())

        agg2=[]
        agg2.append(str(District_B))
        agg2.append(str(Branch_B))
        agg2.append(str(Year))
        agg2.append(df_prod.loc[(df_prod['Year'] == int(Year)) & (df_prod['Sales Office'] == str(Branch_B)), 'Sales per Emp'].sum())
        agg2.append(df_prod.loc[(df_prod['Year'] == int(Year)) & (df_prod['Sales Office'] == str(Branch_B)), 'GM per Emp'].sum())
        agg2.append(df_prod.loc[(df_prod['Year'] == int(Year)) & (df_prod['Sales Office'] == str(Branch_B)), 'Sales Order per Emp'].sum())
        agg2.append(df_prod.loc[(df_prod['Year'] == int(Year)) & (df_prod['Sales Office'] == str(Branch_B)), 'Sales Order Items per Emp'].sum())
        agg2.append(df_prod.loc[(df_prod['Year'] == int(Year)) & (df_prod['Sales Office'] == str(Branch_B)), 'Sales Order Delivery per Emp'].sum())
        agg2.append(df_prod.loc[(df_prod['Year'] == int(Year)) & (df_prod['Sales Office'] == str(Branch_B)), 'Non-Sales Order Delivery per Emp'].sum())
        agg2.append(df_prod.loc[(df_prod['Year'] == int(Year)) & (df_prod['Sales Office'] == str(Branch_B)), 'Delivery Lines per Emp'].sum())
        agg2.append(df_prod.loc[(df_prod['Year'] == int(Year)) & (df_prod['Sales Office'] == str(Branch_B)), 'Deliver Docs per Emp'].sum())
        agg2.append(df_prod.loc[(df_prod['Year'] == int(Year)) & (df_prod['Sales Office'] == str(Branch_B)), 'Employee Count'].mean())

        prod_agg=[agg1]
        prod_agg.append(agg2)

        temp_df_prod_agg=pd.DataFrame(prod_agg, columns = prod_agg_columns)

    #   Profitability Yearly Comparison Code

        prof_agg_columns = ['Sales District Name', 'Sales Office', 'Year',
                            'Number of Delivery Lines (SO)','Sales $ per branch',
                            'GM $ per branch', 'RSO by plant','GMR','Sales Order - RSO']

        agg3=[]
        agg3.append(str(District_A))
        agg3.append(str(Branch_A))
        agg3.append(str(Year))
        agg3.append(df_prof.loc[(df_prof['Year'] == int(Year)) & (df_prof['Sales Office'] == str(Branch_A)), 'Number of Delivery Lines (SO)'].sum())
        agg3.append(df_prof.loc[(df_prof['Year'] == int(Year)) & (df_prof['Sales Office'] == str(Branch_A)), 'Sales $ per branch'].sum())
        agg3.append(df_prof.loc[(df_prof['Year'] == int(Year)) & (df_prof['Sales Office'] == str(Branch_A)), 'GM $ per branch'].sum())
        agg3.append(df_prof.loc[(df_prof['Year'] == int(Year)) & (df_prof['Sales Office'] == str(Branch_A)), 'RSO by plant'].sum())
        agg3.append(df_prof.loc[(df_prof['Year'] == int(Year)) & (df_prof['Sales Office'] == str(Branch_A)), 'GMR'].mean())
        agg3.append(df_prof.loc[(df_prof['Year'] == int(Year)) & (df_prof['Sales Office'] == str(Branch_A)), 'Sales Order - RSO'].sum())

        agg4=[]
        agg4.append(str(District_B))
        agg4.append(str(Branch_B))
        agg4.append(str(Year))
        agg4.append(df_prof.loc[(df_prof['Year'] == int(Year)) & (df_prof['Sales Office'] == str(Branch_B)), 'Number of Delivery Lines (SO)'].sum())
        agg4.append(df_prof.loc[(df_prof['Year'] == int(Year)) & (df_prof['Sales Office'] == str(Branch_B)), 'Sales $ per branch'].sum())
        agg4.append(df_prof.loc[(df_prof['Year'] == int(Year)) & (df_prof['Sales Office'] == str(Branch_B)), 'GM $ per branch'].sum())
        agg4.append(df_prof.loc[(df_prof['Year'] == int(Year)) & (df_prof['Sales Office'] == str(Branch_B)), 'RSO by plant'].sum())
        agg4.append(df_prof.loc[(df_prof['Year'] == int(Year)) & (df_prof['Sales Office'] == str(Branch_B)), 'GMR'].mean())
        agg4.append(df_prof.loc[(df_prof['Year'] == int(Year)) & (df_prof['Sales Office'] == str(Branch_B)), 'Sales Order - RSO'].sum())

        prof_agg=[agg3]
        prof_agg.append(agg4)

        temp_df_prof_agg=pd.DataFrame(prof_agg, columns = prof_agg_columns)
        
        
    #   Monthly Data
    
        temp_df_prod1 = df_prod[(df_prod['Date (YYYYMM)'] == int(timeline)) & (df_prod['Sales Office'] == Branch_A)]
        temp_df_prod2 = df_prod[(df_prod['Date (YYYYMM)'] == int(timeline)) & (df_prod['Sales Office'] == Branch_B)]
        temp_df_prod = pd.concat([temp_df_prod1, temp_df_prod2])
        temp_df_prof1 = df_prof[(df_prof['Date (YYYYMM)'] == int(timeline)) & (df_prof['Sales Office'] == Branch_A)]
        temp_df_prof2 = df_prof[(df_prof['Date (YYYYMM)'] == int(timeline)) & (df_prof['Sales Office'] == Branch_B)]
        temp_df_prof = pd.concat([temp_df_prof1, temp_df_prof2])




    #   Code for Productivity YTD/LYTD

        prod_ytd=pd.DataFrame(columns=prod_columns)
        prod_ytd['Sales District Name']=df_prod['Sales District Name']
        prod_ytd['Sales Office']=df_prod['Sales Office']
        prod_ytd['Date (YYYYMM)']=df_prod['Date (YYYYMM)']
        prod_ytd['Sales per Emp']=df_prod.groupby(['Sales Office','Year'])['Sales per Emp'].cumsum()
        prod_ytd['GM per Emp']=df_prod.groupby(['Sales Office','Year'])['GM per Emp'].cumsum()
        prod_ytd['Sales Order per Emp']=df_prod.groupby(['Sales Office','Year'])['Sales Order per Emp'].cumsum()
        prod_ytd['Sales Order Items per Emp']=df_prod.groupby(['Sales Office','Year'])['Sales Order Items per Emp'].cumsum()
        prod_ytd['Sales Order Delivery per Emp']=df_prod.groupby(['Sales Office','Year'])['Sales Order Delivery per Emp'].cumsum()
        prod_ytd['Non-Sales Order Delivery per Emp']=df_prod.groupby(['Sales Office','Year'])['Non-Sales Order Delivery per Emp'].cumsum()
        prod_ytd['Delivery Lines per Emp']=df_prod.groupby(['Sales Office','Year'])['Delivery Lines per Emp'].cumsum()
        prod_ytd['Deliver Docs per Emp']=df_prod.groupby(['Sales Office','Year'])['Deliver Docs per Emp'].cumsum()
        prod_ytd['Employee Count']=df_prod['Employee Count']

        temp_prod_ytd1 = prod_ytd[(prod_ytd['Date (YYYYMM)'] == int(timeline)) & (prod_ytd['Sales Office'] == Branch_A)]
        temp_prod_ytd2 = prod_ytd[(prod_ytd['Date (YYYYMM)'] == int(timeline)) & (prod_ytd['Sales Office'] == Branch_B)]
        temp_prod_ytd = pd.concat([temp_prod_ytd1, temp_prod_ytd2])

        temp_prod_lytd1 = prod_ytd[(prod_ytd['Date (YYYYMM)'] == (int(timeline)-100)) & (prod_ytd['Sales Office'] == Branch_A)]
        temp_prod_lytd2 = prod_ytd[(prod_ytd['Date (YYYYMM)'] == (int(timeline)-100)) & (prod_ytd['Sales Office'] == Branch_B)]
        temp_prod_lytd = pd.concat([temp_prod_lytd1, temp_prod_lytd2])


    #   Code for Profitability YTD/LYTD

        prof_ytd=pd.DataFrame(columns=prof_columns)
        prof_ytd['Sales District Name']=df_prof['Sales District Name']
        prof_ytd['Sales Office']=df_prof['Sales Office']
        prof_ytd['Date (YYYYMM)']=df_prof['Date (YYYYMM)']
        prof_ytd['Number of Delivery Lines (SO)']=df_prof.groupby(['Sales Office','Year'])['Number of Delivery Lines (SO)'].cumsum()
        prof_ytd['Sales $ per branch']=df_prof.groupby(['Sales Office','Year'])['Sales $ per branch'].cumsum()
        prof_ytd['GM $ per branch']=df_prof.groupby(['Sales Office','Year'])['GM $ per branch'].cumsum()
        prof_ytd['RSO by plant']=df_prof.groupby(['Sales Office','Year'])['RSO by plant'].cumsum()
        prof_ytd['GMR']=df_prof['GMR']
        prof_ytd['Sales Order - RSO']=df_prof.groupby(['Sales Office','Year'])['Sales Order - RSO'].cumsum()

        temp_prof_ytd1 = prof_ytd[(prof_ytd['Date (YYYYMM)'] == int(timeline)) & (prof_ytd['Sales Office'] == Branch_A)]
        temp_prof_ytd2 = prof_ytd[(prof_ytd['Date (YYYYMM)'] == int(timeline)) & (prof_ytd['Sales Office'] == Branch_B)]
        temp_prof_ytd = pd.concat([temp_prof_ytd1, temp_prof_ytd2])

        temp_prof_lytd1 = prof_ytd[(prof_ytd['Date (YYYYMM)'] == (int(timeline)-100)) & (prof_ytd['Sales Office'] == Branch_A)]
        temp_prof_lytd2 = prof_ytd[(prof_ytd['Date (YYYYMM)'] == (int(timeline)-100)) & (prof_ytd['Sales Office'] == Branch_B)]
        temp_prof_lytd = pd.concat([temp_prof_lytd1, temp_prof_lytd2])




# #Outputs the profitability and associated label
# def get_profitability(branch_name, district_name, month, year, scenario_type):

#     return "profitability"

# #Outputs the productivity and associated label
# def get_productivity(branch_name, district_name, month, year, scenario_type):

#     return "productivity"

