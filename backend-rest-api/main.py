import flask
import process_data
from flask import request,  jsonify, make_response

app = flask.Flask(__name__)

@app.route('/')
def home():
    return "works"


@app.route('/v1/branch-to-branch-comparison-table/', methods=["GET"])
def branch_to_branch_comparison():

    branch_a_name = request.args.get('branch_a_name')

    branch_b_name = request.args.get('branch_b_name')

    district_a_name = request.args.get('district_a_name')

    district_b_name = request.args.get('district_b_name')

    productivity_scenario_type = request.args.get('productivity_scenario_type')

    profitability_scenario_type = request.args.get('profitability_scenario_type')

    month = request.args.get('month')

    year = request.args.get('year')

    try:
        response_body = process_data.B2B_Comparison(productivity_scenario_type, profitability_scenario_type, year+month, district_a_name, branch_a_name, district_b_name, branch_b_name).to_json(orient='records')
        res = make_response(response_body, 200)

    # Fix this error code!!
    except:
        res = make_response(jsonify({"Error" : "Invalid parameters"}), 400) 

    return res

